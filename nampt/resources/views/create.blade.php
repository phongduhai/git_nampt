@extends('layouts.default');
@section('title','Create a User')
@section('content')
        <div class="container">
            <form method="POST" action="{{ url('users/') }}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    @if ($errors->has('mail_address'))
                    <label for="name" style="color: red">Mail_address:</label>
                    <input type="text" class="form-control" name="mail_address">
                    <div class="error" style="color: red">{{ $errors->first('mail_address') }}</div>
                    @else
                    <label for="name" >Mail_address:</label>
                    <input type="text" class="form-control" name="mail_address">
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    @if ($errors->has('password'))
                    <label for="name" style="color: red">Password:</label>
                    <input type="password" style="border-color: red" class="form-control" name="password">
                    <div class="error" style="color: red">{{ $errors->first('password') }}</div>
                    @else
                    <label for="name">Password:</label>
                    <input type="password" class="form-control" name="password">
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    @if ($errors->has('password'))
                    <label for="name" style="color: red">Password confirm:</label>
                    <input type="password" style="border-color: red" class="form-control" name="password_confirmation">
                    <div class="error" style="color: red">{{ $errors->first('password_confirmation') }}</div>
                    @else
                    <label for="name">Password confirm:</label>
                    <input type="password" class="form-control" name="password_confirmation">
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    @if ($errors->has('name'))
                    <label for="name" style="color: red">Name:</label>
                    <input type="text" style="border-color: red" class="form-control" name="name">
                    <div class="error" style="color: red">{{ $errors->first('name') }}</div>
                    @else
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="password_confirmation">
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    @if ($errors->has('address'))
                    <label for="name" style="color: red">Address:</label>
                    <input type="text" style="border-color: red" class="form-control" name="name">
                    <div class="error" style="color: red">{{ $errors->first('address') }}</div>
                    @else
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" name="address">
                    @endif    
                    </div>
                </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    @if ($errors->has('phone'))
                    <label for="name" style="color: red">Phone:</label>
                    <input type="text" style="border-color: red" class="form-control" name="phone">
                    <div class="error" style="color: red">{{ $errors->first('phone') }}</div>
                    @else
                    <label for="price">Phone:</label>
                    <input type="text" class="form-control" name="phone">
                    @endif
                </div>
            </div>
        </div>  
            </div>            
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Add Users</button>
              </div>
            </div>
            </form>
        </div>
@stop