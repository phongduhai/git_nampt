<!-- index.blade.php -->

@extends('layouts.default');
@section('title','List User')
@section('content')
        <div class="container">
        <br />
        @include('flash::message')
        <form method="get" action="{{ route('users.index') }}">
            <div class="row col-md-3" >
                
                <div class="form-group" >
                    <label for="name" >Mail_address:</label>
                    <input type="text" class="form-control" name="mail_address">
                </div>
            </div>
            <div class="row col-md-3">
                
                <div class="form-group ">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name">
                </div>
            </div>
            <div class="row col-md-3">
                
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" name="address" value="" >   
                </div>
            </div>
            <div class="row col-md-3">
               
                <div class="form-group">
                    <label for="price">Phone:</label>
                    <input type="text" class="form-control" name="phone">
                </div>
            </div>
            <div class="row col-md-3>
                <div class="form-group col-md-4">
                    <label></label>
                    <div class="form-group col-md-1">
                    <button type="submit" name="search" class="btn btn-success" style="margin-left:38px" value="search" >Search</button>
                    </div>
                </div>    
            </div>
            </div>
            </div>            
        </form>
        </div>  
        <table class="table table-striped">
        <thead>
            <tr>
                <th style="text-align: center;">STT</th>
                <th style="text-align: center;">Địa chỉ email</th>
                <th style="text-align: center;">Tên</th>
                <th style="text-align: center;">Địa chỉ</th>
                <th style="text-align: center;">Số điện thoại</th>
            </tr>
        </thead>
        <tbody>
            @foreach($user as $users)
                <tr>
                    <td style="text-align: left;">{{ $i++ }}</td>
                    <td style="text-align: left;">{{ $users['mail_address'] }} </td>
                    <td style="text-align: left;">{{ ToUpperCase::toUpperCase($users->name) }}</td>
                    <td style="text-align: left;">{{ $users['address'] }}</td>
                    <td style="text-align: left;">{{ $users['phone'] }}</td>
                </tr>
            @endforeach
        </tbody>
        </table>
        {{ $user->links() }}
        <div class="row">
            <div class="col-md-4">  
                <a  class="btn btn-success"  href="{{ route('users.create') }} " style="text-decoration: none; color: white">Create User</a>
            </div>
        </div><br/>
        </div>
@stop