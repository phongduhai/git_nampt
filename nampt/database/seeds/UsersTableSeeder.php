<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('vi_VN');
        $now = Carbon::now();
        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                'mail_address' => $faker->unique()->email,
                'password' => Hash::make(123456),
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->e164PhoneNumber ,
                'created_at' => $now,
                'updated_at'=> $now,
                'deleted_at'=> $now,
            ]);
        }
    }
}
