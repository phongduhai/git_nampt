<?php
namespace App\Helper\Facade;

use Illuminate\Support\Facades\Facade;

class ToUpperCaseFacade extends Facade
{
   	protected static function getFacadeAccessor() 
   	{ 
   		return 'toUpperCase'; 
   	}
}