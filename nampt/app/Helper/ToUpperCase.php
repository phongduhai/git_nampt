<?php
namespace App\Helper;

class ToUpperCase 
{
   	public function toUpperCase($strName)
   	{
      	return mb_strtoupper($strName);
   	}
}
?>