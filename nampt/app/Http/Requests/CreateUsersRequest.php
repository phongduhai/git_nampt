<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mail_address' => 'required|unique:users|max:100|email',
            'password' => 'required|max:255',
            'password_confirmation' => 'required|same:password',
            'name' => 'required|max:255',
            'address' => 'max:255',
            'phone' => 'max:15|regex:/^[0-9_.,() ]+$/',
        ];
    }
}
