<?php

namespace App\Http\Controllers;

Use App\Model\Users;
use App\Http\Requests\CreateUsersRequest;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $user;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Users $user)
    {
        $this->users = $user;
    }

    public function index(Request $request)
    {
        if ($request->has('search')) {
            $user = $this->users->searchUser($request);
            $i = ($user->currentpage()-1)* $user->perpage() + 1;
            return view('index', compact('user', 'i'));
        } else {
            $user = $this->users->getList();
            $i = ($user->currentpage()-1)* $user->perpage() + 1;
            return view('index', compact('user', 'i'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUsersRequest $request)
    {   
        $createUser = $this->users->createUser($request->all());
        flash("Thêm người dùng mới thành công")->success()->important();
        return redirect()->route('users.index');
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
