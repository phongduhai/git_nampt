<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDelete;

class Users extends Model
{
    protected $perPage = 20;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mail_address', 'password', 'address', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getList()
    {
    	return Users::orderBy('mail_address', 'ASC')->paginate();
    }

    public function createUser(array $data)
    {
        return Users::create([
            'mail_address' => $data['mail_address'],
            'password' => bcrypt($data['password']),
            'name' => $data['name'],
            'address' => $data['address'],
            'phone' => $data['phone']
        ]);
    }

    public function searchUser($request)
    {
        $mail = $request['mail_address'];
        $name = $request['name'];
        $address = $request['address'];
        $phone = $request['phone'];
        if (empty($phone)) {
            return $users = Users::where([
                ['mail_address', 'like', "%$mail%"],
                ['name', 'like', "%$name%"],
                ['address', 'like', "%$address%"]
            ])->orderBy('mail_address', 'ASC')->paginate();
        } else {
            return $users = Users::where([
                ['mail_address', 'like', "%$mail%"],
                ['name', 'like', "%$name%"],
                ['address', 'like', "%$address%"],
                ['phone', '=', "$phone"]
            ])->orderBy('mail_address', 'ASC')->paginate();
        }
    }
}
