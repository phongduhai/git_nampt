<?php 

/**
 * undocumented class
 *
 * @package default
 * @author 
 **/

trait Active
{
    public function defindYourSelf()
    {
       return get_class($this); 
    }
}

abstract class Supervisor 
{
    protected $slogan;

    public function saySloganOutLoud()
    {
        echo $this->slogan;
    }
}  

interface Boss
{
    public function checkValidSlogan();
}

class EasyBoss extends Supervisor
{
    use Active; 

    public function setSlogan($string){
        $this->slogan = $string;
    }

    public function checkValidSlogan() 
    {
        return (strpos($this->slogan, 'after') !== false || strpos($this->slogan, 'before') !== false);
    }
}

/**
* 
*/
class UglyBoss implements Boss
{
    use Active;

    public function setSlogan($string)
    {
        $this->slogan = $string;
    }

    public function checkValidSlogan()
    {
        return (strpos($this->slogan, 'after') !== false && strpos($this->slogan, 'before') !== false );
    }
}

$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();

$easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');

$uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');

$easyBoss->saySloganOutLoud(); 
echo "<br>";
$uglyBoss->saySloganOutLoud(); 
echo "<br>";

var_dump($easyBoss->checkValidSlogan()); // true
echo "<br>";
var_dump($uglyBoss->checkValidSlogan()); // true

echo "<br>";
echo 'I am ' . $easyBoss->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $uglyBoss->defindYourSelf(); 

?>
