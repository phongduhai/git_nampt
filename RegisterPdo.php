<html lang="en">
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<?php
$error = array();
$data = array();
require('connect.php');
if (isset($_POST['save-session'])) {
    $data['email_address'] = isset($_POST['email_address']) ? $_POST['email_address'] : null;
    $data['password'] = isset($_POST['password']) ? $_POST['password'] : null;
    $data['password_confirm'] = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : null;
    /**
     * check validate email
     *
     * @param string $strEmail
     * @return boolean
     */
    function is_email($strEmail)
    {
        return (filter_var($strEmail, FILTER_VALIDATE_EMAIL));
    }

    /**
     * check validate password
     *
     * @param string $strPassword
     * @return boolean
     */
    function is_password($strPassword)
    {
        return (preg_match("/^[a-zA-Z1-9 ]*$/", $strPassword));
    }

    /**
     * Register Users
     *
     * @param string $strEmail
     * @param string $strPassword
     * @return boolean
     */
    function registerUser($strEmail, $strPassword, $connection)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $create_at = date('Y-m-d H:i:s');
        $update_at = date('Y-m-d H:i:s');
        $sqlRegister = "INSERT INTO users (mail_address, password, created_at, update_at) VALUES (?, ?, ?, ?)";
        $queryRegister = $connection->prepare($sqlRegister);
        $result = $queryRegister->execute(array($strEmail, md5($strPassword), $create_at, $update_at));
        return $result ;
    }

    if (empty($data['email_address']) && !strlen($data['email_address'])) {

        $error['email_address'] = 'Bạn chưa nhập email';
    } elseif (!is_email($data['email_address'])){

        $error['email_address'] = 'Email không đúng định dạng';
    } elseif (strlen($data['email_address']) >= 255) {

        $error['email_address'] = 'Độ dài không vượt quá 255 kí tự';
    }
    if (empty($data['password']) && !strlen($data['password'])) {

        $error['password'] = 'Bạn chưa nhập password';
    } elseif (!is_password($data['password'])) {

        $error['password'] = 'password không đúng định dạng';
    } elseif (strlen($data['password']) <= 6 && strlen($data['password']) >= 50) {

        $error['email_address'] = 'Độ dài password không nhỏ hơn 6 và lớn hơn 50 kí tự';
    }
    if (empty($data['password_confirm']) && !strlen($data['password_confirm'])) {

        $error['password_confirm'] = 'Bạn chưa nhập password_confirm';
    } elseif ($data['password_confirm'] != $data['password']) {

        $error['password_confirm'] = 'Password_confirm không khớp với password';
    }
    if (!$error && registerUser($data['email_address'], $data['password'], $conn)) {
        setcookie("success", "Đăng ký thành công!", time()+1, "/","", 0);
        header('Location: LoginPdo.php');
    }else {
        $error['register'] = 'Đăng ký thất bại';
    }
}
?>
<div style="margin-top: 120px;">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Register</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <font color="red">
                                    <?php
                                    echo (isset($error['register']) ? $error['register'] : null)
                                    ?>
                                </font>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="email_address" placeholder="Email" value="" >
                                <font color="red"><?php echo isset($error['email_address']) ? $error['email_address'] : ''; ?></font>
                            </div>
                            <div class="form-group">
                                <input class="form-control"  type="password" name="password"  placeholder="Password" value="">
                                <font color="red"><?php echo isset($error['password']) ? $error['password'] : ''; ?></font>
                            </div>
                            <div class="form-group">
                                <input class="form-control"  type="password" name="password_confirm"  placeholder="Password_confirm" value="">
                                <font color="red"><?php echo isset($error['password_confirm']) ? $error['password_confirm'] : ''; ?></font>
                            </div>
                            <button style="margin-top: 30px;" type="submit" name="save-session" class="btn btn-lg btn-success btn-block">Register</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>