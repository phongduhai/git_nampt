<?php
/**
 * HandleString class
 *
 * @package default
 * @author
 */
class HandleString 
{
    //property
    public $check1;
    public $check2;

    /**
     * Read file.
     *
     * @param  int  $x
     * @return string $string
     */
    public function readFile(int $x) 
    {
        $file = @fopen("file$x.txt", "r");
        // Check open file successfully
        if (!$file) {
            echo 'Mở file không thành công';
        } else {
            // Read file and return the content
            $string = fread($file, filesize("file$x.txt"));
        }
        return $string;
    }

    /**
     * check validate string.
     *
     * @param  string  $string   The name
     * @return boolean $success
     */
    public function checkValidString(string $string) 
    {
        $success = false;

        if (strlen($string) == 0) {
            $success = true;
        } elseif (strlen($string) >= 50 && strpos($string, 'after') === false) {
            $success = true;
        } elseif (strpos($string, 'after') === false && strpos($string, 'before') !== false) {
            $success = true;
        }
        return $success;
    }
}

//EX2

$object1 = new HandleString();
$string = $object1->readFile('1');
$object1->check1 = $object1->checkValidString($string);

$string = $object1->readFile('2');
$object1->check2 = $object1->checkValidString($string);

?>