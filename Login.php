<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    form {
        border: 3px solid #f1f1f1;
    }

    input[type=text], input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    /* Full-width inputs */
    input[type=email], input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    input[type=submit] {
        background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
    }

    button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
    }

    button:hover {
        opacity: 0.8;
    }

    .cancelbtn {
        width: auto;
        padding: 10px 18px;
        background-color: #f44336;
    }

    .imgcontainer {
        text-align: center;
        margin: 24px 0 12px 0;
    }

    img.avatar {
        width: 40%;
        border-radius: 50%;
    }

    .container {
        padding: 16px;
    }

    span.psw {
        float: right;
        padding-top: 16px;
    }

    /* Change styles for span and cancel button on extra small screens */
    @media screen and (max-width: 300px) {
        span.psw {
           display: block;
           float: none;
        }
        .cancelbtn {
           width: 100%;
        }
    }
</style>
</head>
<body>

<h2>Login Form  </h2>

<?php
// Code PHP Validate
$error = array();
$data = array();
if (!empty($_POST['save_session'])) {
    // Get data

    $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
    $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';

    function is_email($email) {
        return (filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    function is_password($str) {
        return (preg_match("/^[a-zA-Z1-9 ]*$/", $str));
    }

    if (empty($data['email'])) {
        $error['email'] = 'Bạn chưa nhập email';
    } elseif (!is_email($data['email'])) {
        $error['email'] = 'Email không đúng định dạng';
    } elseif ($data['email'] != 'nampt@gmail.com') {
        $error['email'] = ' * Sai thông tin đăng nhập';
    }

    if (empty($data['password'])) {
        $error['password'] = 'Bạn chưa nhập password';
    } elseif (!is_password($data['password'])) {
        $error['password'] = 'Password không đúng định dạng';
    } elseif (strlen($data['password']) <= 6 || strlen($data['password']) >= 255) {
        $error['password'] = ' Mật khau không đúng định dạng';
    } elseif ($data['password'] != 'nampt123') {
        $error['password'] = ' * Sai mật khau';
    }

    // Save data
    if (!$error) {
        if (isset($_POST["remember"])) {
            setcookie("email_login", $_POST["email"], time() + (10 * 365 * 24 * 60 * 60));
            setcookie("password", $_POST["password"], time() + (10 * 365 * 24 * 60 * 60));
        }
        $_SESSION["name"] = 'nampt@gmail.com';

        header("Location: LoginSuccess.php");
    } else {
        $error['error'] = ' Đăng nhập thất bại ';
    }
}
?>
<label style="color: red" ><b> <?php echo isset($error['error']) ? $error['error'] : ''; ?>  </b></label>
<form method="POST" action="Login.php">
    <div class="container">
        <label><b>Email</b> </label>
        <label style="color: red" ><b> <?php echo isset($error['email']) ? $error['email'] : ''; ?>  </b></label>
        <input type="email" placeholder="Enter Email" name="email" id="email" value="<?php if (isset($_COOKIE["email_login"])) {echo $_COOKIE["email_login"];}?>" required>

        <label><b>Password</b> </label>
        <label style="color: red" ><b> <?php echo isset($error['password']) ? $error['password'] : ''; ?>  </b></label>
        <input type="password" placeholder="Enter Password" name="password" value="<?php if (isset($_COOKIE["password"])) {echo $_COOKIE["password"];}?>" required>
        <input type="submit" name="save_session" value="Login">
        <label> <input type="checkbox" checked="checked" name="remember" > Remember me </label>
    </div>
</form>

</body>
</html>
