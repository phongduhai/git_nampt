<?php
    session_start();
    if (!isset($_SESSION['email_address'])) {
        header('Location: LoginPdo.php');
    }
    if (isset($_POST['logout'])) {
        setcookie("emailCookie", "", time()-7200);
        setcookie("passwordCookie", "", time()-7200);
        session_destroy();
        header('Location: LoginPdo.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="utf-8">
</head>
<body>
    Đăng nhập thành công với tài khoản <?php echo $_SESSION['email_address'] ?>

    <form method="POST">
        <input type="submit" name="logout" value="Logout">
    </form>
</body>
</html>

