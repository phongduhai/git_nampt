<?php

//Ex 1

/**
 * checkValidString
 *
 * @param string $string
 * @return boolean $success
 **/
function checkValidString($string) 
{
    $success = false;

    if (strlen($string) == 0) {
        $success = true;
    } elseif (strlen($string) >= 50 && strpos($string, 'after') === false) {
        $success = true;
    } elseif (strpos($string, 'after') === false && strpos($string, 'before') !== false) {
        $success = true;
    }
    return $success;

}

//Ex 2

for ($x = 1; $x < 3; $x++) {
    $file = @fopen("file$x.txt", "r");
    // Check open file successfully
    if (!$file) {
        echo 'Mở file không thành công';
    } else {
        // Read file and return the content
        $string = fread($file, filesize("file$x.txt"));
        if (checkValidString($string)) {
            echo "File$x có chuỗi hợp lệ <br>";
        } else {
            echo "File$x có chuỗi không hợp lệ <br>";
        }
    }
}
?>