<?php
session_start();
if (isset($_POST['logout'])) {
    session_destroy();
    header('Location: Login.php');
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div><?php if (isset($_SESSION['name'])) { echo 'Đăng nhập thành công';} ?></div>
        <form method="POST" action="LoginSuccess.php">
            <input type="submit" name="logout" value=" <?php if (isset($_SESSION['name'])) { echo 'Logout';} else {echo 'Back';}?>" />
        </form>
    </body>
</html>
