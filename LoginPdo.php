<html lang="en">
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<?php
$error = array();
$data = array();
require('connect.php');
if (isset($_POST['save-session'])) {
    $data['email_address'] = isset($_POST['email_address']) ? $_POST['email_address'] : null;
    $data['password'] = isset($_POST['password']) ? $_POST['password'] : null;
    /**
     * check validate email_address
     *
     * @param string $strEmail
     * @return boolean
     */
    function is_email($strEmail)
    {
        return (filter_var($strEmail, FILTER_VALIDATE_EMAIL));
    }

    /**
     * check validate password
     *
     * @param string $strPassword
     * @return boolean
     */
    function is_password($strPassword)
    {
        return (preg_match("/^[a-zA-Z1-9 ]*$/", $strPassword));
    }

    /**
     * Check login
     *
     * @param string $strEmail
     * @param string $strPassword
     * @return boolean
     */
    function checkLogin($strEmail, $strPassword, $connection)
    {
        $sqlLogin = "SELECT mail_address, password FROM users WHERE mail_address=? AND password=?";
        $queryLogin = $connection->prepare($sqlLogin);
        $queryLogin->execute(array($strEmail, md5($strPassword)));
        return ($queryLogin->rowCount() >= 1);
    }

    if (empty($data['email_address']) && !strlen($data['email_address'])) {

        $error['email_address'] = 'Bạn chưa nhập email';
    } elseif (!is_email($data['email_address'])){

        $error['email_address'] = 'Email không đúng định dạng';
    } elseif (strlen($data['email_address']) >= 255) {

        $error['email_address'] = 'Độ dài không vượt quá 255 kí tự';
    }
    if (empty($data['password']) && !strlen($data['password'])) {

        $error['password'] = 'Bạn chưa nhập password';
    } elseif (!is_password($data['password'])) {

        $error['password'] = 'password không đúng định dạng';
    } elseif (strlen($data['password']) <= 6 && strlen($data['password']) >= 50) {

        $error['email'] = 'Độ dài password không nhỏ hơn 6 và lớn hơn 50 kí tự';
    }
    if (!$error && checkLogin($data['email_address'], $data['password'], $conn)) {
        session_start();
        $_SESSION['email_address'] = $_POST['email_address'];
        $_SESSION['password'] = $_POST['password'];
        if (isset($_POST['remember'])) {
            setcookie('emailCookie', $_POST['email_address'], time()+7200);
            setcookie('passwordCookie', $_POST['password'], time()+7200 );
        }
        header('Location: LoginSuccessPdo.php');
    }else {
        $error['login'] = 'Đăng nhập thất bại';
    }
}
if (isset($_COOKIE['emailCookie']) && isset($_COOKIE['passwordCookie'])) {
    session_start();
    $_SESSION['email_address'] = $_COOKIE['emailCookie'];
    $_SESSION['password'] = $_COOKIE['passwordCookie'];
    header('Location: LoginSuccessPdo.php');
}
?>
<div style="margin-top: 120px;">
    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="margin:auto;">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"  >Please Sign In</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <font color="red">
                                    <?php
                                    echo (isset($_COOKIE['success']) ? $_COOKIE['success'] : null);
                                    echo (isset($error['login']) ? $error['login'] : null)
                                    ?>
                                </font>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="email_address" placeholder="Email_address" value="" >
                                <font color="red"><?php echo isset($error['email_address']) ? $error['email_address'] : ''; ?></font>
                            </div>
                            <div class="form-group">
                                <input class="form-control"  type="password" name="password"  placeholder="Password" value="">
                                <font color="red"><?php echo isset($error['password']) ? $error['password'] : ''; ?></font>
                            </div>
                            <div class="form-group checkbox-inline ">
                                <label>
                                    <input type="checkbox" name="remember" value="remember"> Remember Me 
                                </label>
                            </div>
                            <div class="form-group">
                                <a href="RegisterPdo.php">Đăng ký tài khoản » </a>
                            </div>

                            <button style="margin-top: 30px;" type="submit" name="save-session" class="btn btn-lg btn-success btn-block">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>